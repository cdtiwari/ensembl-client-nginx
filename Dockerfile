FROM nginx:1.14

LABEL maintainer="kamal@ebi.ac.uk"

EXPOSE 80 8000

COPY ensembl-client-nginx/config/conf.d /etc/nginx/conf.d/

COPY src/ensembl/dist /usr/staticfiles